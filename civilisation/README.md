# Principe

## AgentState

Cette classe peut aussi s'appeler message, elle permet de faire remonter
l'action d'un `Citizen` à une classe de plus haut niveau.

## View

Correspond à la perception d'un `Citizen`:

- Un morceau de la carte d'une certaine taille avec le citoyen au `center`.
- la position du citoyen sur le bout de carte.
- Les alliés proches.
- Les ennemies proches.

## Policy TODO

Gère les probas.
