#ifndef __POLICY__
#define __POLICY__

class Policy {
  // NOTE: les poids sont compris entre 0 et 100;
private:
  int fightWeight;
  int interactWeight;
  int explorationWeight;
  int captureWeight;
  int captureWeightIncrement;

public:
  int  fight();
  int  interact();
  int  capture();
  int  exploration();
  int  captureIncrement() { return captureWeightIncrement; }
  void setFight(int);
  void setInteract(int);
  void setCapture(int);
  void setExploration(int);
  Policy(int fightWeight, int interactWeight, int explorationWeight,
         int captureWeight, int captureWeightIncrement)
      : fightWeight(fightWeight), interactWeight(interactWeight),
        explorationWeight(explorationWeight), captureWeight(captureWeight),
        captureWeightIncrement(captureWeightIncrement) {}
};

#endif
