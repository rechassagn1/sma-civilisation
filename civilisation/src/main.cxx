#include "Agent.hxx"
#include "AgentState.hxx"
#include "Citizen.hxx"
#include "Civilisation.hxx"
#include "Policy.hxx"
#include "Simulator.hxx"
#include <cstdlib>
#include <iostream>
#include <new>
#include <random>

int main(int, char **) {
  Map                 *map = new Map(20, 20);
  std::list<Citizen *> blue{new Citizen(BLUE, 2, 2), new Citizen(BLUE, 2, 3)};
  std::list<Citizen *> red{new Citizen(RED, 18, 18),
                           new Citizen(RED, 19, 19)};
  std::list<Citizen *> green{new Citizen(GREEN, 0, 18),
                             new Citizen(GREEN, 1, 19)};
  Civilisation *civB = new Civilisation(BLUE, blue, Policy(50, 10, 10, 10, 20));
  Civilisation *civR = new Civilisation(RED, red, Policy(50, 10, 10, 10, 20));
  Civilisation *civV = new Civilisation(GREEN, green, Policy(50, 10, 10, 10, 20));

  Simulator simu(map, std::list<Civilisation *>{civR, civB, civV});
  simu.run();
  return 0;
}
