#include "Policy.hxx"

int Policy::fight() { return fightWeight; }

int Policy::interact() { return interactWeight; }

int Policy::capture() { return captureWeight; }

int Policy::exploration() { return explorationWeight; }

void Policy::setFight(int fightWeight) { this->fightWeight = fightWeight; }

void Policy::setInteract(int interactWeight) {
  this->interactWeight = interactWeight;
}

void Policy::setCapture(int captureWeight) {
  this->captureWeight = captureWeight;
}

void Policy::setExploration(int explorationWeight) {
  this->explorationWeight = explorationWeight;
}
