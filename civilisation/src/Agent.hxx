#ifndef __AGENT__
#define __AGENT__
#include <iostream>

enum COLOR { RED, GREEN, BLUE, WHITE };

class Agent {
protected:
  COLOR color;

public:
  COLOR      getColor();
  void        setColor(COLOR);
  std::string toString();
  Agent(COLOR);
  virtual ~Agent() = 0;
};

#endif
