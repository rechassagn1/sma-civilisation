#include "Agent.hxx"
#include <sstream>

/******************************************************************************/
/*                                constructors                                */
/******************************************************************************/

Agent::Agent(COLOR color) : color(color) {}

Agent::~Agent() {}

/******************************************************************************/
/*                                 accessors                                  */
/******************************************************************************/

COLOR Agent::getColor() { return color; }

void Agent::setColor(COLOR color) { this->color = color; }

/******************************************************************************/
/*                                 to string                                  */
/******************************************************************************/

std::string Agent::toString() {
  std::ostringstream oss;
  switch (color) {
  case RED:
    oss << "RED";
    break;
  case GREEN:
    oss << "GREEN";
    break;
  case BLUE:
    oss << "BLUE";
    break;
  case WHITE:
    oss << "WHITE";
    break;
  }
  return oss.str();
}
