#include "Point.hxx"
#include <sstream>

Point::Point(int row, int column) : _row(row), _column(column) {}

int &Point::column() { return _column; }

int &Point::row() { return _row; }

int Point::getColumn() const { return _column; }

int Point::getRow() const { return _row; }

/**
 * @brief  Computre the distance^2 between the current point and the given one.
 *         NOTE: we use this to compare distances between points, here we don't
 *         need the real distance so there is no need to compute the square
 *         root.
 *
 * @param  point  Other point.
 *
 * @return  distance^2
 */
int Point::distance2(Point point) {
  int diffx = _column - point.column();
  int diffy = _row - point.row();
  return diffx * diffx + diffy * diffy;
}

std::string Point::toString() {
  std::ostringstream oss;
  oss << "(" << _row << ", " << _column << ")";
  return oss.str();
}

bool Point::operator!=(const Point &p) {
  return _column != p.getColumn() || _row != p.getRow();
}

bool Point::operator==(const Point &p) { return this->operator!=(p); }

std::ostream &operator<<(std::ostream &stream, const Point &p) {
  stream << "(row: " << p.getRow() << ", column: " << p.getColumn() << ")";
  return stream;
}
