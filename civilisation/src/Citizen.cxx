#include "Citizen.hxx"
#include "Agent.hxx"
#include "AgentState.hxx"
#include "Point.hxx"
#include "Test.hxx"
#include "View.hxx"
#include <functional>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

/******************************************************************************/
/*                              mersenne twister                              */
/******************************************************************************/

std::mt19937 g_gen32;

/******************************************************************************/
/*                                constructor                                 */
/******************************************************************************/

Citizen::Citizen(COLOR color, int row, int column)
    : Agent(color), position(row, column), busy(false), age(0) {}

/******************************************************************************/
/*                                  methodes                                  */
/******************************************************************************/

AgentState Citizen::makeAction(View *view, Policy policy) {
  std::vector<Citizen *> closeAllies;
  std::vector<Citizen *> closeEnemies;
  std::vector<Point>     availablePositions;
  int                    capturedCellNumber;

  // the citizen dies if it's too old
  if (age == AGE_MAX)
    return AgentState(position, ACTION::DIE);
  ++age;

  capturedCellNumber = view->getCapturedCellNumber();
  closeAllies = view->getAllies();
  closeEnemies = view->getEnemies();
  availablePositions = view->getAvailablePositions();

  // calcul des poids
  int fightP = policy.fight() * closeEnemies.size();
  int interP = (availablePositions.size() > 0)
                   ? policy.interact() * closeAllies.size()
                   : 0;
  int colonP =
      (view->get(position.row(), position.column()).getColor() != color)
          ? policy.capture() + policy.captureIncrement() * capturedCellNumber
          : 0;
  int exploP = policy.exploration() * availablePositions.size();
  int totalP = fightP + interP + colonP + exploP;

  // the citizen dies if there no action possible
  if (totalP == 0)
    return AgentState(position, ACTION::DIE);

  int decision = g_gen32() % totalP;

  if (decision < fightP) {
    return fight(closeEnemies);
  } else if (decision < fightP + interP) {
    return interact(closeAllies);
  } else if (decision < fightP + interP + colonP) {
    return capture();
  } else {
    return move(view, policy);
  }
}

/**
 * @brief  Use to test if the point `other` is nearer than `currentNearest` from
 *         the citizen.
 *
 * @param  citizen         Position of the citizen.
 * @param  currentNearest  Current nearest point from the citizen.
 * @param  other           Other point to compare with `currentNearest`.
 *
 * @return  true if `other` is nearer.
 */
int nearer(Point p, Point currentNearest, Point other) {
  return p.distance2(currentNearest) > p.distance2(other);
}

Point nearest(Point p, std::vector<Point> points) {
  Point out = points.back();
  points.pop_back();

  for (Point curr : points) {
    if (nearer(p, out, curr)) {
      out = curr;
    }
  }
  return out;
}

// this is used by `computeMoveDatas` to report the results.
typedef struct {
  // fight
  Point bestFightPosition;
  int   fightWeight;
  // interaction
  Point bestInterPosition;
  int   interWeight;
  // capture
  Point bestColonPosition;
  int   colonWeight;
} moveDatas_t;

/**
 * @brief  For each available position around the citizen, computes the number
 *         of accessible allies, enemies or captured cells around. This will be
 *         used to chose the future position of the citizen. For instance, if
 *         among the available positions, many of theme are surround by enemies,
 *         if the policy is in favor of fighting, the citizen will chose the
 *         position that is surround by the greatest number of enemies.
 *
 * @param  view   View of the citizen.
 * @param  color  Color of the citizen.
 *
 * @return  Result of the environment's studdy.
 */
moveDatas_t computeMoveDatas(View *view, COLOR color) {
  // fight
  Point bestFight(-1, -1);
  int   bestFightWeight = -1;
  int   sumFight = 0;
  // interaction
  Point bestInter(-1, -1);
  int   bestInterWeight = -1;
  int   sumInter = 0;
  // capture
  Point bestColon(-1, -1);
  int   bestColonWeight = -1;
  int   sumColon = 0;

  for (Point position : view->getAvailablePositions()) {
    int r1 = std::max(position.row() - 1, 0),
        r2 = std::min(view->getMaxH() - 1, position.row() + 1);
    int c1 = std::max(position.column() - 1, 0),
        c2 = std::min(view->getMaxW() - 1, position.column() + 1);
    // current weights
    int currFightWeight = 0;
    int currInterWeight = 0;
    int currColonWeight = 0;
    // the current position is not colonized already
    bool whitePosition =
        view->get(position.row(), position.column()).getColor() == WHITE;

    // loop on the cells around the position
    for (int row = r1; row <= r2; ++row) {
      for (int column = c1; column <= c2; ++column) {
        Citizen *c = view->get(row, column).getCitizen();
        bool     isCaptured = view->get(row, column).getColor() == color;

        if (c != nullptr && c->getColor() != color) {
          currFightWeight++;
        } else if (c != nullptr && c != view->getCitizen() &&
                   c->getColor() == color) {
          currInterWeight++;
        }
        if (whitePosition && isCaptured) {
          currColonWeight++;
        }
      }
    }

    // update the sums
    sumFight += currFightWeight;
    sumInter += currInterWeight;
    sumColon += currColonWeight;

    // update the bests
    if (currFightWeight > bestFightWeight) {
      bestFightWeight = currFightWeight;
      bestFight = position;
    }
    if (currInterWeight > bestInterWeight) {
      bestInterWeight = currInterWeight;
      bestInter = position;
    }
    if (currColonWeight > bestColonWeight) {
      bestColonWeight = currColonWeight;
      bestColon = position;
    }
  }

  return (moveDatas_t){.bestFightPosition = bestFight,
                       .fightWeight = sumFight,
                       .bestInterPosition = bestInter,
                       .interWeight = sumInter,
                       .bestColonPosition = bestColon,
                       .colonWeight = sumColon};
}

/**
 * @brief  Chose a destination for the citizen and output the corresponding
 *         State.
 *
 * @param  view    View of the citizen.
 * @param  policy  Policy of the civilisation.
 *
 * @return  State with the new position.
 */
AgentState Citizen::move(View *view, Policy policy) {
  AgentState state;
  state.setAction(ACTION::MOVE);
  std::vector<Point> enemiesTarget;
  std::vector<Point> friendTarget;
  std::vector<Point> cellsTarget;

  // this should never happen
  if (view->getAvailablePositions().size() <= 0)
    throw std::logic_error("the availablePositions should not be emtpy.");

  // init variables for the environment composition
  moveDatas_t moveDatas = computeMoveDatas(view, color);

  int fightP = policy.fight() * moveDatas.fightWeight;
  int interP = policy.interact() * moveDatas.interWeight;
  int colonP = policy.capture() * moveDatas.colonWeight;
  int exploP = policy.exploration();
  int totlaP = fightP + interP + colonP + exploP;

  // take decision
  if (totlaP > 0) {
    int decision = g_gen32() % totlaP;

    if (decision < fightP) {
      state.setTargetCoordinates(moveDatas.bestFightPosition);
      return state;
    } else if (decision < fightP + interP) {
      state.setTargetCoordinates(moveDatas.bestInterPosition);
      return state;
    } else if (decision < fightP + interP + colonP) {
      state.setTargetCoordinates(moveDatas.bestColonPosition);
      return state;
    }
  }

  // returns random coordinates if it can chose a new position logically
  int random = g_gen32() % view->getAvailablePositions().size();
  state.setTargetCoordinates(view->getAvailablePositions()[random]);

  return state;
}

/**
 * @brief  Capture cell.
 *
 * @return State for capturing the current cell.
 */
AgentState Citizen::capture() { return AgentState(position, ACTION::CAPTURE); }

/**
 * @brief  Interaction avec un des aliers à proximité.
 *
 * @param  allies  Les alliés à proximité.
 *
 * @return Un état qui permet de signifier qu'il y a eu une intéraction avec un
 *         allié positionné à un certain endroit.
 */
AgentState Citizen::interact(std::vector<Citizen *> allies) {
  int random = g_gen32() % allies.size();
  return AgentState(allies[random]->getPosition(), ACTION::INTERACT);
}

/**
 * @brief  Choose an enemy to fight.
 *
 * @param  enemies  Enemies around the citizen.
 *
 * @return State with a fight action and the coordinates of the targeted enemy.
 */
AgentState Citizen::fight(std::vector<Citizen *> enemies) {
  int random = g_gen32() % enemies.size();
  return AgentState(enemies[random]->getPosition(), ACTION::FIGHT);
}

/******************************************************************************/
/*                                 to string                                  */
/******************************************************************************/

std::string Citizen::toString() {
  std::ostringstream oss;
  oss << "Citizen: " << Agent::toString() << ", " << position.toString();

  return oss.str();
}

/******************************************************************************/
/*                                 accessors                                  */
/******************************************************************************/

void Citizen::setPosition(Point position) { this->position = position; }

Point Citizen::getPosition() { return position; }

bool &Citizen::isBusy() { return busy; }

/******************************************************************************/
/*                                   print                                    */
/******************************************************************************/

void Citizen::print() {
  switch (color) {
  case RED:
    std::cout << "RR";
    break;
  case GREEN:
    std::cout << "VV";
    break;
  case BLUE:
    std::cout << "BB";
    break;
  case WHITE: // this is not possible
    break;
  }
}
