#ifndef __SIMULATOR__
#define __SIMULATOR__
#include "Agent.hxx"
#include "Cell.hxx"
#include "Civilisation.hxx"
#include "Map.hxx"
#include "View.hxx"

class Simulator {
private:
  Map                      *map;
  View                     *view;
  std::list<Civilisation *> civilisations;

public:
  void          step(); // one step into the simulation
  void          run();  // run the simulation
  Civilisation *getCivilisation(COLOR);
  void          deleteCitizen(Citizen *);
  Simulator(Map *, std::list<Civilisation *>);
  Simulator() = default;
  ~Simulator();

private:
  void handleFights(std::list<std::pair<Citizen *, Citizen *>> fighters);
  void
  handleInteractions(std::list<std::pair<Citizen *, Citizen *>> interactions);
};

#endif
