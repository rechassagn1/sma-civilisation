#include "Cell.hxx"
#include "Citizen.hxx"
#include <sstream>

/******************************************************************************/
/*                                constructors                                */
/******************************************************************************/

Cell::Cell(Citizen *citizen, COLOR color) : citizen(citizen), color(color) {}

Cell::Cell() : Cell(nullptr, WHITE) {}

/******************************************************************************/
/*                                 accessors                                  */
/******************************************************************************/

Citizen *Cell::getCitizen() { return citizen; }

COLOR Cell::getColor() { return color; }

void Cell::setCitizen(Citizen *citizen) { this->citizen = citizen; }

void Cell::setColor(COLOR color) { this->color = color; }

/******************************************************************************/
/*                               other methods                                */
/******************************************************************************/

std::string Cell::toString() {
  std::ostringstream oss;
  if (citizen != nullptr) {
    oss << "C";
  } else {
    oss << color;
  }
  return oss.str();
}

void Cell::print() {
  switch (color) {
  case RED:
    std::cout << "\033[0;41m";
    break;
  case GREEN:
    std::cout << "\033[0;42m";
    break;
  case BLUE:
    std::cout << "\033[0;44m";
    break;
  case WHITE:
    std::cout << "\033[0;40m"; // white
    break;
  }
  if (citizen) {
    citizen->print();
  } else {
    std::cout << "  ";
  }
  std::cout << "\033[0m"; // back to normal
}
