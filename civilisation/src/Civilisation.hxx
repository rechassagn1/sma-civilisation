#ifndef __CIVILISATION__
#define __CIVILISATION__
#include "Citizen.hxx"
#include "Policy.hxx"
#include <list>

constexpr int nbCivilisation = 2;

class Civilisation: public Agent
{
  private:
    std::list<Citizen*> citizens;
    Policy policy;

  public:
    Policy getPolicy() const;
    Policy& getPolicy();
    std::list<Citizen*> getCitizens() const;
    void addCitizen(Citizen*);
    void removeCitizen(Citizen*);
    Civilisation();
    Civilisation(COLOR, std::list<Citizen*>, Policy);
    ~Civilisation();
};

#endif
