#include "AgentState.hxx"

AgentState::AgentState(Point coordinates, ACTION action)
    : targetCoordinates(coordinates), action(action) {}

ACTION AgentState::getAction() { return action; }

Point AgentState::getTargetCoordinates() { return targetCoordinates; }
