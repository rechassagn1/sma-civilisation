#ifndef __CELL__
#define __CELL__

#include "Agent.hxx"
#include <string>

class Citizen;

class Cell {
private:
  Citizen *citizen;
  COLOR   color;

public:
  Cell();
  Cell(Citizen *, COLOR);
  void           setCitizen(Citizen *);
  void           setColor(COLOR);
  std::string    toString();
  const Citizen *operator=(Citizen *citizen) { return this->citizen = citizen; }
  COLOR         operator=(const COLOR color) { return this->color = color; }
  Citizen       *getCitizen();
  COLOR         getColor();
  std::string    displayStr();
  void           print();
};

#endif
