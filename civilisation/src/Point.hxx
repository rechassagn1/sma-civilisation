#ifndef __POINT__
#define __POINT__
#include <iostream>

class Point {
private:
  int _row;
  int _column;

public:
  int         getRow() const;
  int         getColumn() const;
  int        &column();
  int        &row();
  int         distance2(Point);
  bool        operator!=(const Point &);
  bool        operator==(const Point &);
  const Point& operator=(const Point& p) {
    _row = p._row;
    _column = p._column;
    return *this;
  }
  std::string toString();
  Point(int, int);
  Point(const Point& p) : _row(p._row), _column(p._column) {}
  ~Point() = default;
};

std::ostream &operator<<(std::ostream &, const Point &);

#endif
