#ifndef __VIEW__
#define __VIEW__
#include "Agent.hxx"
#include "Cell.hxx"
#include "Citizen.hxx"
#include "Map.hxx"
#include "Point.hxx"
#include <vector>

class Citizen;

class View {
private:
  Map                   *map;
  Point                  upleft;
  Citizen               *citizen;
  int                    h;
  int                    w;
  int                    capturedCellNumber;
  std::vector<Citizen *> allies;
  std::vector<Citizen *> enemies;
  std::vector<Point>     availablePositions;

public:
  const Citizen* getCitizen() const { return citizen; }
  Point                  getUpleft() const;
  Cell                   get(int, int); // peut pas mettre const :/
  int                    getH() const;
  int                    getW() const;
  int                    getMaxH() const { return map->getHeight(); }
  int                    getMaxW() const { return map->getWidth(); }
  std::vector<Citizen *> getEnemies() const;
  std::vector<Citizen *> getAllies() const;
  int                    getCapturedCellNumber() const;
  std::vector<Point>     getAvailablePositions() const;
  void                   update(Citizen *);
  void                   setAlliesEnemies();
  View(Map *, Citizen *);
  View(Map *);
  ~View();
};

#endif
