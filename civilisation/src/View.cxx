#include "View.hxx"
#include "Agent.hxx"
#include "Test.hxx"
#include <cstdlib>

/******************************************************************************/
/*                          constructor & destructor                          */
/******************************************************************************/

View::View(Map *map, Citizen *c) : map(map), upleft(0, 0), citizen(c) {}

View::View(Map *map) : View(map, nullptr) {}

View::~View() {}

/******************************************************************************/
/*                                 accessors                                  */
/******************************************************************************/

int View::getH() const { return h; }

int View::getW() const { return w; }

Point View::getUpleft() const { return upleft; }

std::vector<Citizen *> View::getAllies() const { return allies; }

std::vector<Citizen *> View::getEnemies() const { return enemies; }

std::vector<Point> View::getAvailablePositions() const {
  return availablePositions;
}

int View::getCapturedCellNumber() const { return capturedCellNumber; }

Cell View::get(int row, int column) { return map->get(row, column); }

/******************************************************************************/
/*                               helper methods                               */
/******************************************************************************/

void View::update(Citizen *c) { // au pire go prendre un citoyen
  if (!map) {
    std::cout << "map null!";
    exit(1);
  }
  citizen = c;
  map->view25(citizen->getPosition().row(), citizen->getPosition().column(),
              upleft, w, h);
  setAlliesEnemies();
}

void View::setAlliesEnemies() {
  Citizen *curr;
  int      row = citizen->getPosition().row();
  int      column = citizen->getPosition().column();
  int      r1 = std::max(row - 1, 0), r2 = std::min(getMaxH() - 1, row + 1);
  int c1 = std::max(column - 1, 0), c2 = std::min(getMaxW() - 1, column + 1);

  enemies.clear();
  allies.clear();
  availablePositions.clear();
  capturedCellNumber = 0;

  for (int row = r1; row <= r2; row++) {
    for (int column = c1; column <= c2; column++) {
      curr = map->get(row, column).getCitizen();

      if (map->get(row, column).getColor() == citizen->getColor())
        capturedCellNumber++;

      if (curr != nullptr) {
        if (curr != citizen && curr->isBusy() == false) {
          if (curr->getColor() == citizen->getColor()) {
            allies.push_back(curr);
          }

          else {
            enemies.push_back(curr);
          }
        }
      } else {
        availablePositions.push_back(Point(row, column));
      }
    }
  }
}
