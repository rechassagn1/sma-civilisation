#ifndef __CITOYEN__
#define __CITOYEN__
#include <random>
#include "Agent.hxx"
#include "AgentState.hxx"
#include "Policy.hxx"
#include "Cell.hxx"

extern std::mt19937 g_gen32;
// extern int(*g_gen32)();

int constexpr AGE_MAX = 30;

class View;

class Citizen: public Agent
{
  private:
    Point position;
    bool busy;
    int age;

  public:
    bool& isBusy();
    void setPosition(Point);
    AgentState makeAction(View*, Policy);
    Point getPosition();
    std::string toString();
    void print();
    Citizen(COLOR, int, int);
    ~Citizen() = default;

  private:
    AgentState move(View* view, Policy policy);
    AgentState capture();
    AgentState fight(std::vector<Citizen*>);
    AgentState interact(std::vector<Citizen*>);
};

#endif
