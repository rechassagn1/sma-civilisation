#ifndef __TEST__
#define __TEST__
#include <iostream>
#include <string>
#define BEGIN_TEST(A) Test::beginTest(A, __LINE__)
#define END_TEST() Test::endTest()
#define CHECK(A) Test::check(A, __LINE__, #A)
#define FAILED Test::hasFailures()

class Test {
private:
  static std::string currentTest;
  static int         nbTest;
  static int         failures;

public:
  static void beginTest(std::string msg, int line) {
    currentTest = msg;
    nbTest = 0;
    failures = 0;
    nbTest = 0;
    std::cout << line << " - START: " << msg << std::endl;
  }
  static void endTest() {
    std::cout << "END: " << currentTest << std::endl;
    std::cout << "\t";
    if (nbTest > failures)
      std::cout << "\033[0;32m";
    std::cout << nbTest - failures << " passed\033[0m / ";
    if (failures > 0)
      std::cout << "\033[0;31m";
    std::cout << failures << " failed\033[0m"
              << " (" << nbTest << ")" << std::endl;
    currentTest = "";
  }
  static void check(bool result, int line, std::string test) {
    nbTest++;
    if (!result) {
      failures++;
      std::cout << "\033[0;31m";
      std::cout << line << " - ERROR: " << test << std::endl;
      std::cout << "\033[0m"; // back to normal
    }
  }
  static bool hasFailures() {
    return failures > 0;
  }
  Test() = default;
  ~Test() = default;
};

#endif
