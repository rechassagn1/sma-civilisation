#ifndef __AGENT_STATE__
#define __AGENT_STATE__
#include "Point.hxx"

/**
 * @brief  Represents the actions that a citizen can make when it plays.
 */
enum class ACTION { MOVE, CAPTURE, FIGHT, INTERACT, DIE };

class AgentState {
private:
  // coordinates on moving
  Point   targetCoordinates;
  ACTION action;

public:
  void setAction(ACTION action) { this->action = action; }
  void setTargetCoordinates(Point targetCoordinates) {
    this->targetCoordinates = targetCoordinates;
  }
  Point   getTargetCoordinates();
  ACTION getAction();
  AgentState(Point, ACTION);
  AgentState() : targetCoordinates(-1, -1), action(ACTION::MOVE) {}
  ~AgentState() = default;
};

#endif
