#include "Simulator.hxx"
#include "AgentState.hxx"
#include "Citizen.hxx"
#include "Test.hxx"
#include <sstream>
#include <stdexcept>
#include <thread>

/******************************************************************************/
/*                         constructor and destructor                         */
/******************************************************************************/

Simulator::Simulator(Map *map, std::list<Civilisation *> civilisations)
    : map(map), civilisations(civilisations) {
  for (Civilisation *civ : civilisations) {
    for (Citizen *cit : civ->getCitizens()) {
      map->get(cit->getPosition()).setCitizen(cit);
    }
  }
  view = new View(map);
}

Simulator::~Simulator() {
  for (Civilisation *civilisation : civilisations) {
    delete civilisation;
  }
  delete view;
  delete map;
}

/******************************************************************************/
/*                                    step                                    */
/******************************************************************************/

/**
 * @brief  For each fight that occured during the last round, choose the winner
 *         of the fight and delete the other citizen. Notice that the attacker
 *         as more chances to win the fight.
 *
 * @param  fighters  List of pair of citizens where the first one is the
 *                   attacker.
 */
void Simulator::handleFights(
    std::list<std::pair<Citizen *, Citizen *>> fighters) {
  for (std::pair<Citizen *, Citizen *> cts : fighters) {
    if (g_gen32() % 100 < 65) {
      deleteCitizen(cts.second);
      cts.first->isBusy() = false;
    } else {
      deleteCitizen(cts.first);
      cts.second->isBusy() = false;
    }
  }
}

/**
 * @brief  For each interaction that occured in the last round, find an
 *         available position and create a new citizen that is place at the
 *         available position.
 *
 * @param  interactions  All interactions that occured in the last round.
 */
void Simulator::handleInteractions(
    std::list<std::pair<Citizen *, Citizen *>> interactions) {
  Citizen *newCitzen = nullptr;

  for (std::pair<Citizen *, Citizen *> cts : interactions) {
    Citizen *c1 = cts.first;
    // coordinates of the 9x9 sqare to look at on the grid:
    int row1 = std::max(c1->getPosition().row() - 1, 0);
    int column1 = std::max(c1->getPosition().column() - 1, 0);
    int row2 = std::min(c1->getPosition().row() + 1, view->getMaxH() - 1);
    int column2 = std::min(c1->getPosition().column() + 1, view->getMaxW() - 1);
    newCitzen = nullptr;
    // search for space to put the new citizen in the map
    for (int row = row1; row <= row2 && newCitzen == nullptr; ++row) {
      for (int column = column1; column <= column2 && newCitzen == nullptr;
           ++column) {
        if (map->get(row, column).getCitizen() == nullptr) {
          newCitzen = new Citizen(c1->getColor(), row, column);
        }
      }
    }

    // add the new citizen
    if (newCitzen) {
      Civilisation *civilisation = getCivilisation(newCitzen->getColor());
      civilisation->addCitizen(newCitzen);
      map->get(newCitzen->getPosition()) = newCitzen;
    }
    cts.first->isBusy() = false;
    cts.second->isBusy() = false;
  }
}

/******************************************************************************/
/*                                    step                                    */
/******************************************************************************/

/**
 * @brief  Make each citizen of each civilisation play a round.
 */
void Simulator::step() {
  AgentState                                 state;
  std::list<std::pair<Citizen *, Citizen *>> fighters;
  std::list<std::pair<Citizen *, Citizen *>> interactions;
  Citizen                                   *target;

  for (Civilisation *civilisation : civilisations) {
    for (Citizen *citizen : civilisation->getCitizens()) {
      if (!citizen->isBusy()) {
        view->update(citizen);
        state = citizen->makeAction(view, civilisation->getPolicy());

        switch (state.getAction()) {
        case ACTION::MOVE:
          // test if the citizen has moved
          map->get(citizen->getPosition()).setCitizen(nullptr);
          citizen->setPosition(state.getTargetCoordinates());
          map->get(citizen->getPosition()).setCitizen(citizen);
          break;
        case ACTION::CAPTURE:
          map->get(citizen->getPosition()).setColor(citizen->getColor());
          break;
        case ACTION::FIGHT:
          target = map->get(state.getTargetCoordinates()).getCitizen();
          citizen->isBusy() = true;
          target->isBusy() = true;
          fighters.push_back(std::make_pair(citizen, target));
          break;
        case ACTION::INTERACT:
          citizen->isBusy() = true;
          map->get(state.getTargetCoordinates()).getCitizen()->isBusy() = true;
          interactions.push_back(std::pair<Citizen *, Citizen *>(
              citizen, map->get(state.getTargetCoordinates()).getCitizen()));
          // decrease interaction weight whene there is a lot of citizens
          // NOTE: this was hard to equilibrate, the objective is to increase
          // the interaction at the begining and make it decrease as the
          // population groth. The result is interesting ...
          civilisation->getPolicy().setInteract(
              1 + civilisation->getPolicy().interact() *
                      (5 / (civilisation->getCitizens().size())));
          break;
        case ACTION::DIE:
          deleteCitizen(citizen);
          break;
        }
      }
    }
  }
  handleFights(fighters);
  handleInteractions(interactions);
}

/******************************************************************************/
/*                                    run                                     */
/******************************************************************************/

/**
 * @brief  Run the simulation in an infinit loop.
 */
void Simulator::run() {
  while (1) {
    step();
    system("clear");
    map->printMap();
    /* SLEEP */ {
      using namespace std::chrono_literals;
      std::this_thread::sleep_for(500ms);
    }
  }
}

/******************************************************************************/
/*                                   utils                                    */
/******************************************************************************/

/**
 * @brief  Find the civilisation with the color `color`.
 *
 * @param  color  Color of the civilisation to search for.
 *
 * @return Civilisation found. If no civilisation has been found, returns a
 *         nullptr.
 */
Civilisation *Simulator::getCivilisation(COLOR color) {
  for (Civilisation *civilisation : civilisations) {
    if (civilisation->getColor() == color) {
      return civilisation;
    }
  }
  return nullptr;
}

/**
 * @brief  Remove a citizen from it's civilisatoin and from the map and then
 *         delete it. Raise an exception when the citizen is not the one on
 *         the cell.
 *
 * @param  citizen  Citizen to delete.
 */
void Simulator::deleteCitizen(Citizen *citizen) {
  Civilisation *civilisation = getCivilisation(citizen->getColor());
  civilisation->removeCitizen(citizen);

  if (citizen != map->get(citizen->getPosition()).getCitizen()) {
    std::ostringstream oss;
    oss << "ERROR: wrong citizen position: " << citizen << " "
        << citizen->toString();
    throw std::logic_error(oss.str());
  }

  map->get(citizen->getPosition()) = nullptr;
  delete citizen;
}
