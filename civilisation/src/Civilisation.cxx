#include "Civilisation.hxx"
#include "Agent.hxx"

/******************************************************************************/
/*                          constructor & destructor                          */
/******************************************************************************/

Civilisation::Civilisation()
    : Civilisation(WHITE, std::list<Citizen *>(), Policy(30, 30, 30, 30, 5)) {}

Civilisation::Civilisation(COLOR color, std::list<Citizen *> citizens,
                           Policy policy)
    : Agent(color), citizens(citizens), policy(policy) {}

Civilisation::~Civilisation() {
  for (Citizen *c : citizens) {
    delete c;
  }
}

/******************************************************************************/
/*                                 accessors                                  */
/******************************************************************************/

std::list<Citizen *> Civilisation::getCitizens() const { return citizens; }

Policy Civilisation::getPolicy() const { return policy; }
Policy& Civilisation::getPolicy() { return policy; }

void Civilisation::addCitizen(Citizen *citizen) { citizens.push_back(citizen); }

void Civilisation::removeCitizen(Citizen *citizen) { citizens.remove(citizen); }
