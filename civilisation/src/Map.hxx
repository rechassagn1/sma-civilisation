#ifndef __MAP__
#define __MAP__

#include "Cell.hxx"
#include "Point.hxx"
#include <iostream>
#include <list>

class Map {
private:
  Cell **map;
  int    height;
  int    width;

public:
  int getWidth() const { return width; }
  int getHeight() const { return height; }
  Map(int, int);
  ~Map();
  void   printMap();
  Cell **getMap();
  void   view25(int, int, Point &, int &, int &);
  Cell &get(int, int);
  Cell &get(Point);
};

#endif
