#include "Map.hxx"

Map::Map(int height, int width) : height(height), width(width) {
  map = new Cell *[height];
  for (int i = 0; i < height; i++) {
    map[i] = new Cell[width];
  }
}

Map::~Map() {
  for (int i = 0; i < height; i++) {
    delete[] map[i];
  }
  delete[] map;
}

void Map::printMap()
{
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      map[i][j].print();
    }
    std::cout << std::endl;
  }
}

void Map::view25(int row, int column, Point &p, int &vw, int &vh) {
  Point p2((row + 2 >= height) ? 0 : row + 2,
           (column + 2 >= width) ? 0
                            : column + 2);   // Point bas-droite de la vue accessible
  p.column() = (column - 2 < 0) ? 0 : column - 2; // Point haut-gauche de la vue
  p.row() = (row - 2 < 0) ? 0 : row - 2;    // Point haut-gauche de la vue
  vh = std::min(height - p.row(), row + 2 - p.row() + 1);
  vw = std::min(width - p.column(), column + 2 - p.column() + 1);
}

/******************************************************************************/
/*                                 accessors                                  */
/******************************************************************************/

Cell &Map::get(int row, int column) { return map[row][column]; }

Cell &Map::get(Point p) { return map[p.row()][p.column()]; }

Cell **Map::getMap() { return map; }
