#ifndef TEST_VIEW_HPP
#define TEST_VIEW_HPP
#include "../src/Agent.hxx"
#include "../src/AgentState.hxx"
#include "../src/Citizen.hxx"
#include "../src/Map.hxx"
#include "../src/Policy.hxx"
#include "../src/View.hxx"
#include <cppunit/Test.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class TestView : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TestView);
  CPPUNIT_TEST(testUpdate);
  CPPUNIT_TEST_SUITE_END();

private:
  Citizen *c;
  Citizen *ally;
  Citizen *enemy;
  View    *view;
  Map     *map;

public:
  void setUp() {
    c = new Citizen(BLUE, 4, 4);
    ally = new Citizen(BLUE, 4, 3);
    enemy = new Citizen(GREEN, 3, 3);
    Map *map = new Map(20, 20);
    map->get(4, 4).setCitizen(c);
    map->get(4, 3).setCitizen(ally);
    map->get(3, 3).setCitizen(enemy);
    view = new View(map);
    view->update(c); // updating the view
  }

  void tearDown() {
    delete c;
    delete ally;
    delete enemy;
    delete view;
    delete map;
  }

  void testGet() {
    CPPUNIT_ASSERT(20 == view->getMaxH());
    CPPUNIT_ASSERT(20 == view->getMaxW());
    CPPUNIT_ASSERT(view->get(4, 4).getCitizen() == c);
    CPPUNIT_ASSERT(view->get(4, 3).getCitizen() == ally);
    CPPUNIT_ASSERT(view->get(3, 3).getCitizen() == enemy);
  }

  void testUpdate() {
    CPPUNIT_ASSERT(1 == view->getAllies().size());
    CPPUNIT_ASSERT(1 == view->getEnemies().size());
    CPPUNIT_ASSERT(view->getEnemies().back() == enemy);
    CPPUNIT_ASSERT(view->getAllies().back() == ally);
    CPPUNIT_ASSERT(6 == view->getAvailablePositions().size());
    CPPUNIT_ASSERT(5 == view->getH());
    CPPUNIT_ASSERT(5 == view->getW());
  }
};

#endif
