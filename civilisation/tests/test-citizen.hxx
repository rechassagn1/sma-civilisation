#ifndef __TEST_CITIZEN__
#define __TEST_CITIZEN__
#include "../src/Agent.hxx"
#include "../src/AgentState.hxx"
#include "../src/Citizen.hxx"
#include "../src/Map.hxx"
#include "../src/Policy.hxx"
#include "../src/View.hxx"
#include <cppunit/Test.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// TODO: create a proper cxx file
class TestCitizen : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TestCitizen);
  CPPUNIT_TEST(testColors); // call test method
  CPPUNIT_TEST(testToString);
  CPPUNIT_TEST(testMakeAction);
  CPPUNIT_TEST_SUITE_END();

private:
  Citizen *c;
  Citizen *ally;
  Citizen *enemy;
  View    *view;
  Map     *map;

public:
  void setUp() {
    c = new Citizen(BLUE, 4, 4);
    ally = new Citizen(BLUE, 4, 3);
    enemy = new Citizen(GREEN, 3, 3);
    Map *map = new Map(20, 20);
    map->get(4, 4).setCitizen(c);
    map->get(4, 3).setCitizen(ally);
    map->get(3, 3).setCitizen(enemy);
    view = new View(map);
  }

  void tearDown() {
    delete c;
    delete ally;
    delete enemy;
    delete view;
    delete map;
  }

  void testColors() { CPPUNIT_ASSERT(c->getColor() == BLUE); }

  void testToString() {
    CPPUNIT_ASSERT(c->toString() == "Citizen: BLUE, (4, 4)");
  }

  void testMakeAction() {
    // testing some test
    for (int i = 0; i < 100; ++i) {
      view->update(c);
      AgentState state = c->makeAction(view, Policy(50, 10, 10, 10, 20));
      int        cr = c->getPosition().row();
      int        cc = c->getPosition().column();
      int        sr = state.getTargetCoordinates().row();
      int        sc = state.getTargetCoordinates().column();

      switch (state.getAction()) {
      case ACTION::MOVE:
        // the citizen has moved
        CPPUNIT_ASSERT(sc != cc || sr != cr);
        // the position is located arround the citizen
        CPPUNIT_ASSERT(sr >= cr - 1 && sr <= cr + 1 && sc >= cc - 1 &&
                       sc <= cc + 1);
        // the new position is in the map
        CPPUNIT_ASSERT(sr >= 0 && sr < view->getMaxH());
        CPPUNIT_ASSERT(sc >= 0 && sc < view->getMaxW());
        break;
      case ACTION::CAPTURE:
        // test if the citizen hasn't move
        CPPUNIT_ASSERT(state.getTargetCoordinates().column() ==
                           c->getPosition().column() &&
                       state.getTargetCoordinates().row() == c->getPosition().row());
        break;
      case ACTION::FIGHT:
        CPPUNIT_ASSERT(state.getTargetCoordinates().column() ==
                           enemy->getPosition().column() &&
                       state.getTargetCoordinates().row() == enemy->getPosition().row());
        break;
      case ACTION::INTERACT:
        CPPUNIT_ASSERT(state.getTargetCoordinates().column() ==
                           ally->getPosition().column() &&
                       state.getTargetCoordinates().row() == ally->getPosition().row());
        break;
      case ACTION::DIE:
        CPPUNIT_ASSERT(state.getTargetCoordinates().column() ==
                           c->getPosition().column() &&
                       state.getTargetCoordinates().row() == c->getPosition().row());
      }
    }
  }
};

#endif
