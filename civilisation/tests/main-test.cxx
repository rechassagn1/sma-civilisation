#include "test-citizen.hxx"
#include "test-view.hxx"
#include <cppunit/ui/text/TestRunner.h>

int main(int, char**)
{
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(TestCitizen::suite());
  runner.addTest(TestView::suite());
  return !runner.run("", false);
}
