# sma-civilisation

TP d'architecture logiciel et qualité dont l'objectif été de réaliser une simulation multi agent.

## Build & run

Pour compiler et lancer le projet:

```bash
mkdir -p civilisation/src/build
cd civilisation/src/build
cmake -S .. -B .
make
./civilisation
```

Pour compiler et lancer les tests:

```bash
mkdir -p civilisation/tests/build
cd civilisation/tests/build
cmake -S .. -B .
make
./civ-test
```

**IMPORTANT:** pour compiler les tests, il faut avoir installé CppUnit !
